//Gabriel Damburiarena 889/19
#include <stack>

template <typename T>
string_map<T>::string_map() : _size(0), raiz(nullptr) {
    // COMPLETAR
}

template <typename T>
string_map<T>::string_map(const string_map<T>& aCopiar) : string_map() { *this = aCopiar; } // Provisto por la catedra: utiliza el operador asignacion para realizar la copia.

template <typename T>
string_map<T>& string_map<T>::operator=(const string_map<T>& d) {
    borrar_nodo(raiz);
    raiz = nullptr;
    _size = 0;
    vector<string> clavesOtro = d._claves;
    for (string clave : clavesOtro) {
        insert(make_pair(clave, d.at(clave)));
    }
    return *this;
}

template<class T>
void string_map<T>::borrar_nodo(Nodo* n) {
    if (n != nullptr) {
        for (Nodo* hijo : n->siguientes) {
            borrar_nodo(hijo);
        }
        delete n->definicion;
    }
    delete n;
}

template <typename T>
string_map<T>::~string_map(){
    borrar_nodo(raiz);
    raiz = nullptr;
}

template<class T>
void string_map<T>::insert(const pair<string, T>& par) {
    if (raiz == nullptr) {
        raiz = new Nodo();
    }
    string clave = par.first;
    Nodo* nodoActual = raiz;
    for (int i = 0; i < clave.size(); i++) {
        if (nodoActual->siguientes[int(clave[i])] == nullptr) {
            nodoActual->siguientes[int(clave[i])] = new Nodo();
        }
        nodoActual = nodoActual->siguientes[clave[i]];
    }
    if (nodoActual->definicion == nullptr) {
        _size++;
        _claves.push_back(clave);
    } else {
        delete nodoActual->definicion;
    }
    nodoActual->definicion = new T(par.second);
}

template <typename T>
T& string_map<T>::operator[](const string& clave){
    // COMPLETAR
}

template <typename T>
int string_map<T>::count(const string& clave) const{
    if (raiz == nullptr) {
        return 0;
    }
    Nodo* nodoActual = raiz;
    for (char c : clave) {
        if (nodoActual->siguientes[int(c)] != nullptr) {
            nodoActual = nodoActual->siguientes[int(c)];
        } else {
            return 0;
        }
    }
    if (nodoActual->definicion == nullptr) {
        return 0;
    }
    return 1;
}

template <typename T>
const T& string_map<T>::at(const string& clave) const {
    Nodo* nodoActual = raiz;
    for (char c : clave) {
        nodoActual = nodoActual->siguientes[int(c)];
    }
    return *nodoActual->definicion;
}

template <typename T>
T& string_map<T>::at(const string& clave) {
    Nodo* nodoActual = raiz;
    for (char c : clave) {
        nodoActual = nodoActual->siguientes[int(c)];
    }
    return *nodoActual->definicion;
}

template<class T>
bool string_map<T>::todos_nullptr(vector<Nodo*> siguientes) {
    for (Nodo* nodo : siguientes) {
        if (nodo != nullptr) {
            return false;
        }
    }
    return true;
}

template <typename T>
void string_map<T>::erase(const string& clave) {
    stack<Nodo*> pila;
    Nodo* nodoActual = raiz;
    for (int i = 0; i < _claves.size(); i++) {
        if (_claves[i] == clave){
            _claves.erase(_claves.begin() + i);
            break;
        }
    }
    for (char c : clave) {
        pila.push(nodoActual);
        nodoActual = nodoActual->siguientes[int(c)];
    }
    delete nodoActual->definicion;
    nodoActual->definicion = nullptr;
    int i = clave.length() - 1;
    while(pila.size() > 0) {
        if (nodoActual->definicion == nullptr && todos_nullptr(nodoActual->siguientes)) {
            pila.pop();
            nodoActual = pila.top();
            delete nodoActual->siguientes[int(clave[i])];
            nodoActual->siguientes[int(clave[i])] = nullptr;
            i--;
        } else {
            break;
        }
    }
    _size--;
}

template <typename T>
int string_map<T>::size() const{
    return _size;
}

template <typename T>
bool string_map<T>::empty() const{
    return _size == 0;
}

template<class T>
vector<string> string_map<T>::claves() {
    return _claves;
}