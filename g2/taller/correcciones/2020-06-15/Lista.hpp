// Damburiarena Gabriel - 889/19

#include "Lista.h"

Lista::Lista() : prim_(nullptr), ult_(nullptr), longitud_(0) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (longitud_ > 0) {
        eliminar(0);
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    while (longitud_ > 0) {
        eliminar(longitud_ - 1);
    }
    Nodo* nodoActual = aCopiar.prim_;
    while (nodoActual != nullptr) {
        agregarAtras(nodoActual->dato);
        nodoActual = nodoActual->siguiente;
    }
    return *this;
}

void Lista::agregarAdelante(const int& elem) {
    Nodo* nuevoNodo = new Nodo();
    nuevoNodo->dato = elem;
    if (prim_ == nullptr) {
        prim_ = nuevoNodo;
        ult_ = nuevoNodo;
        longitud_++;
    } else {
        prim_->anterior = nuevoNodo;
        nuevoNodo->siguiente = prim_;
        prim_ = nuevoNodo;
        longitud_++;
    }
}

void Lista::agregarAtras(const int& elem) {
    Nodo* nuevoNodo = new Nodo();
    nuevoNodo->dato = elem;
    if (ult_ == nullptr) {
        prim_ = nuevoNodo;
        ult_ = nuevoNodo;
        longitud_++;
    } else {
        ult_->siguiente = nuevoNodo;
        nuevoNodo->anterior = ult_;
        ult_ = nuevoNodo;
        longitud_++;
    }
}

void Lista::eliminar(Nat i) {
    if (i > longitud_ - 1) {
        return;
    }
    Nodo* nodoActual = prim_;
    while (i != 0) {
        nodoActual = nodoActual->siguiente;
        i--;
    }
    Nodo* ant = nodoActual->anterior;
    Nodo* sig = nodoActual->siguiente;
    if (ant == nullptr) {
        prim_ = sig;
        if (sig != nullptr) {
            sig->anterior = nullptr;
        }
    }
    if (sig == nullptr) {
        ult_ = ant;
        if(ant != nullptr) {
            ant->siguiente = nullptr;
        }
    }
    if (ant != nullptr && sig != nullptr) {
        ant->siguiente = sig;
        sig->anterior = ant;
    }
    delete nodoActual;
    longitud_--;
}

int Lista::longitud() const {
    return longitud_;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* nodoActual = prim_;
    while (i > 0) {
        nodoActual = nodoActual->siguiente;
        i--;
    }
    return nodoActual->dato;
}

int& Lista::iesimo(Nat i) {
    Nodo* nodoActual = prim_;
    while (i > 0) {
        nodoActual = nodoActual->siguiente;
        i--;
    }
    return nodoActual->dato;
}

void Lista::mostrar(ostream& o) {
    Nodo* nodoActual = prim_;
    o << "[";
    while (nodoActual != nullptr) {
        o << nodoActual->dato;
        if (nodoActual->siguiente != nullptr) {
            o << ", ";
        }
        nodoActual = nodoActual->siguiente;
    }
    o << "]";
}

