// Damburiarena Gabriel - 889/19

template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr), _card(0) {
    // Completar
}

template <class T>
Conjunto<T>::~Conjunto() {
    destruir(_raiz);
    _raiz = nullptr;
}

template<class T>
void Conjunto<T>::destruir(Nodo* n) {
    if (n != nullptr) {
        destruir(n->izq);
        destruir(n->der);
        delete n;
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if (_raiz != nullptr) { //Si la raiz es nullptr entonces siempre devuelve false
        Nodo* nodoActual = _raiz;
        while (nodoActual->valor != clave) {
            if (nodoActual->valor > clave) { //Si la clave es menor, hay que mirar en la rama izquierda
                if (nodoActual->izq == nullptr) {
                    return false;
                } else {
                    nodoActual = nodoActual->izq;
                }
            } else if (nodoActual->valor < clave) { //Si la clave es mayor, hay que mirar en la rama derecha
                if (nodoActual->der == nullptr) {
                    return false;
                } else {
                    nodoActual = nodoActual->der;
                }
            }
        }
        return true;
    } else {
        return false;
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* nuevoNodo = new Nodo(clave);
    if (_raiz != nullptr) {
        Nodo* nodoActual = _raiz;
        bool encontrado = false;
        while (!encontrado) {
            if (nodoActual->valor == clave) { //Si el valor ya se encuentra en el ABB, no se inserta nada
                encontrado = true;
            } else if (nodoActual->valor > clave) {
                if (nodoActual->izq == nullptr) { //Se inserta como hijo izquierdo
                    nodoActual->izq = nuevoNodo;
                    _card++;
                    encontrado = true;
                } else {
                    nodoActual = nodoActual->izq;
                }
            } else if (nodoActual->valor < clave) {
                if (nodoActual->der == nullptr) { //Se inserta como hijo derecho
                    nodoActual->der = nuevoNodo;
                    _card++;
                    encontrado = true;
                } else {
                    nodoActual = nodoActual->der;
                }
            }
        }
    } else {
        _raiz = nuevoNodo;
        _card++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (pertenece(clave)) {
        Nodo* nodoActual = _raiz;
        Nodo* nodoAnterior = nullptr;
        bool esHijoDer;
        while (nodoActual->valor != clave) { //Este ciclo busca el nodo a remover
            if (nodoActual->valor > clave) {
                nodoAnterior = nodoActual;
                nodoActual = nodoAnterior->izq;
                esHijoDer = false;
            } else if (nodoActual->valor < clave) {
                nodoAnterior = nodoActual;
                nodoActual = nodoAnterior->der;
                esHijoDer = true;
            }
        }
        if (nodoActual->der == nullptr && nodoActual->izq == nullptr) { //Caso en el que el nodo no tiene hijos
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nullptr;
                } else {
                    nodoAnterior->izq = nullptr;
                }
                delete nodoActual;
            } else {
                delete _raiz;
                _raiz = nullptr;
            }
        } else if (nodoActual->der != nullptr && nodoActual->izq != nullptr) { //Caso en el que el nodo tiene dos hijos
            Nodo* nodoPredInmediato = nodoActual->izq;
            while (nodoPredInmediato->der != nullptr) { //Este ciclo busca el predecesor inmediato
                nodoPredInmediato = nodoPredInmediato->der;
            }
            T predInmediato = nodoPredInmediato->valor;
            remover(nodoPredInmediato->valor); //Se borra el predecesor inmediato ya que tiene que cambiar de lugar
            _card++;
            nodoActual->valor = predInmediato;
        } else if (nodoActual->der != nullptr) { //Caso en el que tiene solo hijo derecho
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nodoActual->der;
                } else {
                    nodoAnterior->izq = nodoActual->der;
                }
                delete nodoActual;
            } else {
                _raiz = nodoActual->der;
                delete nodoActual;
            }
        } else { //Ultimo caso, solo hijo izquierdo
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nodoActual->izq;
                } else {
                    nodoAnterior->izq = nodoActual->izq;
                }
                delete nodoActual;
            } else {
                _raiz = nodoActual->izq;
                delete nodoActual;
            }
        }
        _card--;
    }
}

template<class T>
vector<typename Conjunto<T>::Nodo*> Conjunto<T>::inOrder() { //Devuelve los elementos del conjunto en orden
    stack<Nodo*> pila;
    Nodo* nodoActual = _raiz;
    vector<Nodo*> res;
    while (nodoActual != nullptr || pila.empty() == false) {
        while (nodoActual != nullptr) {
            pila.push(nodoActual);
            nodoActual = nodoActual->izq;
        }
        nodoActual = pila.top();
        pila.pop();
        res.push_back(nodoActual);
        nodoActual = nodoActual->der;
    }
    return res;
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    vector<Nodo*> ConjOrdenado = inOrder();
    for(int i = 0; i < ConjOrdenado.size(); i++) {
        if (ConjOrdenado[i]->valor == clave)
            return ConjOrdenado[i+1]->valor;
    }
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* nodoActual = _raiz;
    while (nodoActual->izq != nullptr) {
        nodoActual = nodoActual->izq;
    }
    return nodoActual->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* nodoActual = _raiz;
    while (nodoActual->der != nullptr) {
        nodoActual = nodoActual->der;
    }
    return nodoActual->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _card;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}