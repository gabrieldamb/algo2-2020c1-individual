// Damburiarena Gabriel - 889/19

template <class T>
Conjunto<T>::Conjunto() : _raiz(nullptr), _card(0) {
    // Completar
}

template <class T>
Conjunto<T>::~Conjunto() {
    while(_card > 0) {
        remover(maximo());
    }
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    if (_raiz != nullptr) {
        Nodo* nodoActual = _raiz;
        while (nodoActual->valor != clave) {
            if (nodoActual->valor > clave) {
                if (nodoActual->izq == nullptr) {
                    return false;
                } else {
                    nodoActual = nodoActual->izq;
                }
            } else if (nodoActual->valor < clave) {
                if (nodoActual->der == nullptr) {
                    return false;
                } else {
                    nodoActual = nodoActual->der;
                }
            }
        }
        return true;
    } else {
        return false;
    }
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    Nodo* nuevoNodo = new Nodo(clave);
    if (_raiz != nullptr) {
        Nodo* nodoActual = _raiz;
        bool encontrado = false;
        while (!encontrado) {
            if (nodoActual->valor == clave) {
                encontrado = true;
            } else if (nodoActual->valor > clave) {
                if (nodoActual->izq == nullptr) {
                    nodoActual->izq = nuevoNodo;
                    _card++;
                    encontrado = true;
                } else {
                    nodoActual = nodoActual->izq;
                }
            } else if (nodoActual->valor < clave) {
                if (nodoActual->der == nullptr) {
                    nodoActual->der = nuevoNodo;
                    _card++;
                    encontrado = true;
                } else {
                    nodoActual = nodoActual->der;
                }
            }
        }
    } else {
        _raiz = nuevoNodo;
        _card++;
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if (pertenece(clave)) {
        Nodo* nodoActual = _raiz;
        Nodo* nodoAnterior = nullptr;
        bool esHijoDer;
        while (nodoActual->valor != clave) {
            if (nodoActual->valor > clave) {
                nodoAnterior = nodoActual;
                nodoActual = nodoAnterior->izq;
                esHijoDer = false;
            } else if (nodoActual->valor < clave) {
                nodoAnterior = nodoActual;
                nodoActual = nodoAnterior->der;
                esHijoDer = true;
            }
        }
        if (nodoActual->der == nullptr && nodoActual->izq == nullptr) {
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nullptr;
                } else {
                    nodoAnterior->izq = nullptr;
                }
            } else {
                _raiz = nullptr;
            }
            delete nodoActual;
        } else if (nodoActual->der != nullptr && nodoActual->izq != nullptr) {
            Nodo* nodoPredInmediato = nodoActual->izq;
            while (nodoPredInmediato->der != nullptr) {
                nodoPredInmediato = nodoPredInmediato->der;
            }
            T predInmediato = nodoPredInmediato->valor;
            remover(nodoPredInmediato->valor);
            _card++;
            nodoActual->valor = predInmediato;
        } else if (nodoActual->der != nullptr) {
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nodoActual->der;
                } else {
                    nodoAnterior->izq = nodoActual->der;
                }
            } else {
                _raiz = nodoActual -> der;
            }
            delete nodoActual;
        } else {
            if (nodoAnterior != nullptr) {
                if (esHijoDer) {
                    nodoAnterior->der = nodoActual->izq;
                } else {
                    nodoAnterior->izq = nodoActual->izq;
                }
            } else {
                _raiz = nodoActual->izq;
            }
            delete nodoActual;
        }
        _card--;
    }
}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* nodoActual;
    Nodo* nodoAnterior;
    bool esHijoDer;
    T sig = _raiz->valor;
    if (clave >= _raiz->valor) {
        nodoActual = _raiz;
    } else {
        nodoAnterior = _raiz;
        nodoActual = _raiz->izq;
    }
    while (nodoActual->valor != clave) {
        if (nodoActual->valor > clave) {
            nodoAnterior = nodoActual;
            nodoActual = nodoAnterior->izq;
            esHijoDer = false;
        } else if (nodoActual->valor < clave) {
            nodoAnterior = nodoActual;
            nodoActual = nodoAnterior->der;
            esHijoDer = true;
        }
    }
    if (nodoActual->der == nullptr) {
        if (!esHijoDer) {
            return nodoAnterior->valor;
        } else {
            Nodo* nIter;
            T tActual = clave;
            while (true) {
                nIter = _raiz;
                while (nIter->valor != clave) {
                    if (nIter->valor > clave) {
                        if (nIter->izq->valor == tActual) {
                            return nIter->valor;
                        }
                        nIter = nIter->izq;
                    } else if (nIter->valor < clave) {
                        if (nIter->der->valor == tActual) {
                            tActual = nIter->valor;
                        }
                        nIter = nIter->der;
                    }
                }
            }

        }
    }
    nodoActual = nodoActual->der;
    while (nodoActual->izq != nullptr) {
        nodoActual = nodoActual->izq;
    }
    return nodoActual->valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo* nodoActual = _raiz;
    while (nodoActual->izq != nullptr) {
        nodoActual = nodoActual->izq;
    }
    return nodoActual->valor;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* nodoActual = _raiz;
    while (nodoActual->der != nullptr) {
        nodoActual = nodoActual->der;
    }
    return nodoActual->valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _card;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}